import { useState, useEffect, Fragment } from 'react'
import { Form, Button } from 'react-bootstrap'

export default function Login() {

	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [isActive, setIsActive] = useState(false)

	function authenticate(e){
		e.preventDefault()
		setEmail('')
		setPassword('')

		alert(`${email} has been verified! Welcome back!`)
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])


	return (
	<Fragment>
		<h1>Log In</h1>
		 <Form onSubmit={(e) => authenticate(e)}>
            <Form.Group controlId="loginEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
        			onChange={(e) => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
        			onChange={(e) => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

           { isActive ? 
           	<Button variant="success" type="submit" id="loginBtn" className= "mt-3 mb-3">
             Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="loginBtn" disabled className= "mt-3 mb-3">
              Submit
             </Button>
        	}
 		</Form>
 	</Fragment>


	)
}

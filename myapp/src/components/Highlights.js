import { Row, Col, Card} from 'react-bootstrap'

export default function Highlights() {
	return(
		<Row>
		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Learn From Home</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum officia rerum reiciendis temporibus quis cum nulla molestias perferendis laborum maxime doloribus mollitia sint, repudiandae nisi eos consequatur libero non, laudantium!
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Study Now, Pay Later</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Maxime pariatur quas, deleniti mollitia voluptatum quaerat aliquid inventore, maiores tempore voluptatibus dolor itaque quasi nulla, in voluptates laboriosam, velit esse earum?
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>

		<Col xs={12} md={4}>
			<Card className="cardHighlight p-3">
				<Card.Body>
					<Card.Title>
						<h2>Be Part of our Community</h2>
					</Card.Title>
					<Card.Text>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum officia rerum reiciendis temporibus quis cum nulla molestias perferendis laborum maxime doloribus mollitia sint, repudiandae nisi eos consequatur libero non, laudantium!
					</Card.Text>
				</Card.Body>
			</Card>
		</Col>
		</Row>
	)
}
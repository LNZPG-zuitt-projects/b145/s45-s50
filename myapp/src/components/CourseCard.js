import { useState, useEffect } from 'react'
import { Card, Button } from 'react-bootstrap';

export default function CourseCard({courseProp}) {

    // console.log(props)
    // console.log(typeof props)
    console.log(courseProp)

    // deconstruct the courseProp into their own variables
    const {name, description, price} = courseProp

    // Syntax
        // const [getter, setter] = useState(initialValueOfGetter)
    const [count, setCount] = useState(0)
    const [seats, setSeats] = useState(30)

    function enroll(){
        if (seats > 0) {
            setCount(count + 1);
            console.log('Enrollees: ' + count);
            setSeats(seats - 1);
            console.log('Seats: ' + seats);
        }
    }

    useEffect(() => {
        if(seats === 0){
            alert('No more seats available')
        }
    }, [seats])


    return (
        <Card className= "mt-3">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text> 
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}

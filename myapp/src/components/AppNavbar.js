import { Navbar, Nav } from 'react-bootstrap'

export default function AppNavbar() {
	return(
		<Navbar bg="light" expand="lg">
			<Navbar.Brand className= "m-3">Zuitt</Navbar.Brand>
			<Navbar.Toggle/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="#courses">Courses</Nav.Link>
				</Nav>
			</Navbar.Collapse>
			
		</Navbar>
	);
};